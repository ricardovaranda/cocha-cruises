import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormViewComponent } from './Components/form-view/form-view.component';
import { DetailViewComponent } from './Components/detail-view/detail-view.component';
import { AppRoutingModule } from './app-routing.module';

import { ProductService } from './Service/product.service';
import { CurrencyMoney, ComaInPoint } from './Pipes/custom.pipe';
import { ItineraryBlockComponent } from './Components/itinerary-block/itinerary-block.component';
import { PriceBlockComponent } from './Components/price-block/price-block.component';

import { UICarouselModule } from "ui-carousel";

//import { MatButtonModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    FormViewComponent,
    DetailViewComponent,
    CurrencyMoney,
    ComaInPoint,
    ItineraryBlockComponent,
    PriceBlockComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UICarouselModule,
    BrowserAnimationsModule
  ],
  providers: [
    ProductService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
