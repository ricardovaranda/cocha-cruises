import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { ProductInfo } from '../Class/Product-info';
import { DetalleProducto } from '../Class/exampleData';

@Injectable()
export class ProductService {
  constructor() { }

  getProduct(_id): Observable<ProductInfo>{
    return of(DetalleProducto);
  }
}
