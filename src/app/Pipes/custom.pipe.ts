import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'currencyMoney'})
export class CurrencyMoney implements PipeTransform {
  transform(value: string): string {
    let newStr: string = "";
    switch (value){
        case "USD":
            newStr = "US$";
            break;
        case "CLP":
            newStr = "$";
            break;
        default:
            break;
    }
    return newStr;
  }
}

@Pipe({name: 'comaInPoint'})
export class ComaInPoint implements PipeTransform {
  transform(value: string): string {
    let newStr: string = "";
    let re = /\,/gi; 
    newStr = value.replace(re,".");
    return newStr;
  }
}