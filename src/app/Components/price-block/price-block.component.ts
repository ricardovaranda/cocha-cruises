import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-price-block',
  templateUrl: './price-block.component.html',
  styleUrls: ['./price-block.component.scss']
})
export class PriceBlockComponent implements OnInit {
  @Input() day: number;
  @Input() mainPrice: number;
  @Input() secondPrice: number;
  @Input() discount: number;
  @Input() type: string;

  constructor() { }
  ngOnInit() {
  }

}
