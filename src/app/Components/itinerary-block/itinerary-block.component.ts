import { Component, OnInit, Input } from '@angular/core';
import { Itinerary } from '../../Class/Itinerary';

@Component({
  selector: 'app-itinerary-block',
  templateUrl: './itinerary-block.component.html',
  styleUrls: ['./itinerary-block.component.scss']
})
export class ItineraryBlockComponent implements OnInit {
  @Input() itinerary: Itinerary[];
  showItems = 12;
  isClassVisible: false;
  isItineraryVisible: boolean = false;

  onClickisItineraryVisible(){
      this.isItineraryVisible = !this.isItineraryVisible;       
  }
  constructor() {
 
  }
  ngOnInit() {
  }

}
