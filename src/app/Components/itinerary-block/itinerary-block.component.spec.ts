import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItineraryBlockComponent } from './itinerary-block.component';

describe('ItineraryBlockComponent', () => {
  let component: ItineraryBlockComponent;
  let fixture: ComponentFixture<ItineraryBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItineraryBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItineraryBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
