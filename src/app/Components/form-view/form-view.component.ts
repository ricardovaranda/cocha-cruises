import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute } from '@angular/router';

import { trigger, transition, style, animate, stagger, query } from '@angular/animations';

@Component({
  selector: 'app-form-view',
  templateUrl: './form-view.component.html',
  styleUrls: ['./form-view.component.scss'],
  animations: [
    trigger(
      'myAnimation',
      [
        transition('* => *', [
          query(':leave', [
            stagger('10000ms', [
              style({ transform: 'translateX(100%)', 'opacity': 1 }),
              animate('100ms', style({ transform: 'translateX(-100%)', 'opacity': 0 }))
            ])
          ], { optional: true }
        ),
          query(':enter', [
              style({ transform: 'translateX(100%)', opacity: 0 }),
              animate('500ms', style({ transform: 'translateX(0)', 'opacity': 1 }))

            ], { optional: true }
          )
        ])]
    ),
    trigger('fadeInOut', [
      transition('void => *', [
        style({ opacity: 0 }), // Style only for transition transition (after transiton it removes)
        animate(400, style({ opacity: 1 })) // the new state of the transition(after transiton it removes)
      ]),
      transition('* => void', [
        animate(400, style({ opacity: 0 })) // the new state of the transition(after transiton it removes)
      ])
    ])
  ]
})
export class FormViewComponent implements OnInit {

  @ViewChild('ciudades') ciudades: any;

  public idCruise: number;
  public wizardSteps: number;
  public wizardAnimation: boolean;
  public bindingVar = '';
  public isDropdownVisible = false;
  public adults: number;
  public kids: number;
  public regionName = '';
  public cabinType = '';
  public consultant = '';
  public name: string;
  public textValue: string;

  public validationErrors = false;

  constructor(
    private route: ActivatedRoute
  ) {
    this.idCruise = +this.route.snapshot.paramMap.get('idCruise') || null;
    this.wizardSteps = 1;
    this.adults = 1;
    this.kids = 0;
  }

  onNext() {
    if (this.checkValidation(this.wizardSteps)) {
      this.wizardSteps++;
    }
  }

  checkValidation(currentStep) {
    this.validationErrors = false;
    switch (currentStep) {
      case 1:
        if (this.ciudades.nativeElement.value !== '') {
          return true;
        }
        this.validationErrors = true;
        return false;
      case 2:
      case 3:
        return true;
      case 4:
        if (this.cabinType !== '') {
          return true;
        }
        this.validationErrors = true;
        return false;
      case 5:
        this.wizardAnimation = true;
        break;
      default:
      return false;
    }
  }

  onBack() {
    if (this.wizardSteps > 1) {
      this.wizardSteps = this.wizardSteps - 1;
      this.wizardAnimation = false;
    }
  }

  onClickIsDropdownVisible() {
    this.isDropdownVisible = !this.isDropdownVisible;
  }

  onAddAdults() {
    this.adults = this.adults + 1;
  }

  onRemoveAdults() {
    if (this.adults > 1) {
      this.adults = this.adults - 1;
    }
  }

  onAddKids() {
    this.kids = this.kids + 1;
  }

  onRemoveKids() {
    if (this.kids > 0) {
      this.kids = this.kids - 1;
    }
  }

  onClickSetRegionName() {
    this.regionName = this.regionName;
  }

  onClickCabinType() {
    this.cabinType = this.cabinType;
  }

  onClickHasConsultant() {
    this.consultant = this.consultant;
  }


  ngOnInit() {
  }

  getFormBackgroundDesktop() {
    return 'assets/img/Desktop/formulario-desktop.jpg';
  }

  getFormBackgroundMobile() {
    return 'assets/img/Mobile/formulario-mobile.jpg';
  }

  // ANIMATION CONFIG BEGINS

  fadeIn() {
    this.bindingVar = 'fadeIn';
  }
  fadeOut() {
    this.bindingVar = 'fadeOut';
  }
  toggle() {
    this.bindingVar === 'fadeOut' ? this.fadeIn() : this.fadeOut();
  }

  textAreaEmpty() {
    if (this.textValue !== '') {
      console.log(this.textValue);
    }
  }

}
