import { PriceType } from "./Price-type";

export class Price {
    main: PriceType;
    second: PriceType;
}
