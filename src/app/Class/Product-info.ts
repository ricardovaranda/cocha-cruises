import { Itinerary } from "./Itinerary";
import { Price } from "./Price";
import { Image } from "./Image";
import { Place } from "./Place";
import { Amenity } from "./Amenity";
import { Cabin } from "./Cabin";
import { Cruises } from "./Cruises";

export class ProductInfo {
    id: number;

    image: Image;
    title: string;
    cruise: string;
    company: string;
    days: number;
      
    price: Price;
    discount: number;
    
    description: string;
    destinations: Place[];
    itinerary: Itinerary[];

    images: Image[];
    amenities: Amenity[];

    cabin: Cabin[];

    //El objeto cruceros podria cambiar
    similar: Cruises[];
}