import { Image } from "./Image";
import { Place } from "./Place";

export class Itinerary {
    day: number;
    image: Image;
    destination?: Place;
    departure?: string;
    arrival?: string;
    optional?: string;
}
