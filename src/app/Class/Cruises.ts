import { Image } from "./Image";
import { Price } from "./Price";
import { Place } from "./Place";

export class Cruises {
    id: number;
    title: string;
    subtitle: string;
    image: Image;
    days: number;
    price: Price;
    discount: number;
    cities: Place[];
}
