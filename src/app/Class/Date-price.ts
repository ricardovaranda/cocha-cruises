export class DatePrice {
    date: string;
    price: number;
    category: string;
}
